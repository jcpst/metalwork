---
title: About Metalwork
date: 2016-03-15
layout: post.jade
collection: blog
---

Metal<strong>work</strong> is a built out [Metalsmith](//metalsmith.io) blog that works.

Metal<strong>smith</strong> is a framework for creating static sites built by Ian Storm Taylor at Segment.io. 

A common frustration people have with Metalsmith is how flexible it is; you can build almost anything with it. Yet, Metalsmith doesn't ship with any built-out examples. Being that I've used Metalsmith to build my sites/blog/books since early 2015, I decided to go ahead and create a working example based on the blog I run at [evbogue.com](//evbogue.com)

Metalwork is built using Node.js, Jade, Framework.css, and served using Koa.js. It uses the following Metalsmith plugins: metalsmith-collections, metalsmith-permalinks, metalsmith-markdown, metalsmith-layouts, and metalsmith-beautify.

You can view the code at [Gitlab](//gitlab.com/ev/metalwork/)

Assuming you know how to install Node.js and use `npm`, getting Metalwork up and running is simple:

        % npm install metalwork
        % cd node_modules/metalwork
        % node index.js
	Metalwork is running at http://localhost:3000/

Navigate your browser to [localhost:3000](//localhost:3000) and explore you new blog.

To customize metalwork edit the files in `src/` and `layouts/`


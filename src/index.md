---
title: A built-out Metalsmith blog that works
layout: index.jade
---

Hola! I'm Ev Bogue. I'm a programmer and technical writer. All of my stuff fits into one bag. 

Right now I live in Fayetteville, North Carolina. I work at a brewpub and code/write in my spare time.

I'm the author of <a href='/minimalist/'>The Art of Being Minimalist</a> and <a href='/node/'>Node.js | The Distributed Web</a>.   

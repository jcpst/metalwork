#!/usr/bin/env node
'use strict';

const path = require('path');
const argv = require('minimist')(process.argv.slice(2));
const sh = require('shelljs');

const basename = argv._[0] || '';
const destDir = path.join(process.cwd(), basename);
const srcFiles = [
  'index.js',
  'package.json',
  'src',
  'layouts',
  'readme.md'
];

const help = `\
Usage:
  metalwork [dir]

Scaffold out a basic Metalwork site into new directory.`;

const noPathProvided = `\
ERROR: Provide a [path] argument:
$ metalwork new-site`;

function fullPath (basename) {
  return path.join(__dirname, basename);
}

function build (src, dest) {
  sh.mkdir('-p', dest);
  sh.cp('-r', src.map(x => fullPath(x)), dest);
}

/*
 * Entry Point
 */
if (argv.h || argv.help) {
  console.log(help);
} else if (!basename) {
  console.log(noPathProvided);
} else {
  build(srcFiles, destDir);
}

